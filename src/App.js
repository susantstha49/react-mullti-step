import "./App.scss";
import Index from "./components/index";
import SuccessMessage from "./components/successMessage";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <div className="headerWrapper">
        <div className="">
          <div className="mainHeader">
            <div className="logo">
              <img src="/logo-web.png" width="180px" alt="" />
            </div>
          </div>
        </div>
      </div>

      <Router>
        <Route exact path="/">
          <Redirect to="multi-step-form" />
        </Route>
        <Switch>
          <Route path="/multi-step-form/:id?">
            <Index />
          </Route>
          <Route path="/multi-step-success">
            <SuccessMessage />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
