import React, { useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
  updatedata,
  resetForm,
} from "./../../reduxStore/actions/productAction";
import Select from "react-select";

const options = [
  { value: ".net", label: ".NET" },
  { value: ".net-core", label: ".NET Core" },
  { value: "abap", label: "ABAP" },
];

const FormEight = ({ formData, stepChange, handleChange }) => {
  const dispatch = useDispatch();
  const {
    setValue,
    reset,
    formState: { errors },
    handleSubmit: handleSubmit,
    watch,
    control,
  } = useForm({
    mode: "all",
  });

  const onSubmit = (data) => {
    const arr = data.technologiesInvolved.map((x) => x.value);
    dispatch(updatedata({ ...data, technologiesInvolved: arr }));
    handleChange("technologiesInvolved", arr);
  };

  const { loading, updateResponse, validations } = useSelector(
    ({ mulitStepFormReducer }) => ({
      updateResponse: mulitStepFormReducer.addUpdateResponse.data,
      loading: mulitStepFormReducer.addUpdateResponse.loading,
      error: mulitStepFormReducer.addUpdateResponse.error,
    }),
    shallowEqual
  );

  useEffect(() => {
    if (Object.keys(formData).length > 0) {
      const tecArray = options.filter((item) =>
        formData?.technologiesInvolved?.includes(item.value)
      );
      reset({
        leadId: formData.leadId,
        step: +formData.step > 8 ? formData.step : 9,
        technologiesInvolved: tecArray,
      });
    }
  }, [formData]);

  useEffect(() => {
    if (Object.keys(updateResponse).length > 0) {
      stepChange(9);
      dispatch(resetForm("data"));
    }
  }, [updateResponse]);
  return (
    <div className="form">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="row">
          <div className="col col-12">
            <h4 className="">Technologies involved</h4>
          </div>
        </div>
        <div className="row mb-4">
          <div className="col-12">
            <Controller
              name="technologiesInvolved"
              defaultValue={watch("technologiesInvolved")}
              isClearable
              control={control}
              rules={{
                required: true,
              }}
              render={({ field }) => (
                <Select
                  {...field}
                  options={options}
                  isMulti
                  className="basic-multi-select"
                  classNamePrefix="select"
                />
              )}
            />
          </div>
        </div>

        <div className="form-actions">
          <button
            type="button"
            className="btn btn-sm btn-success mx-2"
            onClick={() => stepChange(7)}
          >
            Previous
          </button>
          <button type="submit" className="btn btn-sm btn-primary">
            Continue
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormEight;
