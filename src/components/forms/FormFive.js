import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
	updatedata,
	resetForm,
} from "./../../reduxStore/actions/productAction";

const howLong = [
	{ code: "3-months-or-less", name: "3 months or less" },
	{ code: "3-6-months", name: "3-6 months" },
	{ code: "6-12-months", name: "6-12 months" },
	{ code: "ongoing", name: "Ongoing" },
	{ code: "i-dont-know", name: "I don't know" },
];
const FormFive = ({ formData, stepChange, handleChange }) => {
	const dispatch = useDispatch();
	const {
		register,
		reset,
		formState: { errors },
		handleSubmit: handleSubmit,
		watch,
	} = useForm({
		mode: "all",
	});

	const onSubmit = (data) => {
		dispatch(updatedata(data));
	};

	const { loading, updateResponse, validations } = useSelector(
		({ mulitStepFormReducer }) => ({
			updateResponse: mulitStepFormReducer.addUpdateResponse.data,
			loading: mulitStepFormReducer.addUpdateResponse.loading,
			error: mulitStepFormReducer.addUpdateResponse.error,
		}),
		shallowEqual
	);

	useEffect(() => {
		if (Object.keys(formData).length > 0) {
			reset({
				leadId: formData.leadId,
				step: +formData.step > 5 ? formData.step : 6,
				timePeriodOfITProfessionals: formData?.timePeriodOfITProfessionals,
			});
		}
	}, [formData]);

	useEffect(() => {
		if (Object.keys(updateResponse).length > 0) {
			stepChange(6);
			dispatch(resetForm("data"));
		}
	}, [updateResponse]);

	return (
		<div className="form">
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row">
					<div className="col col-12">
						<h4 className="">For how long will you need this engineer?</h4>
					</div>
				</div>
				<div className="row mb-4">
					{howLong.map((item, index) => (
						<div className="col-12" key={index}>
							<div className="form-check">
								<input
									className="form-check-input"
									{...register("timePeriodOfITProfessionals", {
										required: true,
									})}
									type="radio"
									checked={
										watch("timePeriodOfITProfessionals") === item.code
											? true
											: false
									}
									value={item.code}
									defaultValue={watch("timePeriodOfITProfessionals")}
									onChange={(e) =>
										handleChange("timePeriodOfITProfessionals", item.code)
									}
								/>
								<label className="form-check-label" htmlFor={item.code}>
									{item.name}
								</label>
							</div>
						</div>
					))}
					{errors?.timePeriodOfITProfessionals && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.timePeriodOfITProfessionals.type}
						</small>
					)}
				</div>

				<div className="form-actions">
					<button
						type="button"
						className="btn btn-sm btn-success mx-2"
						onClick={() => stepChange(4)}
					>
						Previous
					</button>
					<button type="submit" className="btn btn-sm btn-primary">
						Continue
					</button>
				</div>
			</form>
		</div>
	);
};

export default FormFive;
