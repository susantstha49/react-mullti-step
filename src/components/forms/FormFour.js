import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
	updatedata,
	resetForm,
} from "./../../reduxStore/actions/productAction";

const timeCommitment = [
	{ code: "a-few-hours-a-week", name: "A few hours a week" },
	{ code: "part-time", name: "Part-time" },
	{ code: "full-time", name: "Full-time" },
];

const FormFour = ({ formData, stepChange, handleChange }) => {
	const dispatch = useDispatch();
	const {
		register,
		reset,
		formState: { errors },
		handleSubmit: handleSubmit,
		watch,
	} = useForm({
		mode: "all",
	});

	const onSubmit = (data) => {
		dispatch(updatedata(data));
	};

	const { loading, updateResponse, validations } = useSelector(
		({ mulitStepFormReducer }) => ({
			updateResponse: mulitStepFormReducer.addUpdateResponse.data,
			loading: mulitStepFormReducer.addUpdateResponse.loading,
			error: mulitStepFormReducer.addUpdateResponse.error,
		}),
		shallowEqual
	);

	useEffect(() => {
		if (Object.keys(formData).length > 0) {
			reset({
				leadId: formData.leadId,
				step: +formData.step > 4 ? formData.step : 5,
				timeCommitmentOfDeveloper: formData?.timeCommitmentOfDeveloper,
			});
		}
	}, [formData]);

	useEffect(() => {
		if (Object.keys(updateResponse).length > 0) {
			stepChange(5);
			dispatch(resetForm("data"));
		}
	}, [updateResponse]);

	return (
		<div className="form">
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row">
					<div className="col col-12">
						<h4 className="">
							What level of time commitment will you require from the developer?
						</h4>
					</div>
				</div>
				<div className="row mb-4">
					{timeCommitment.map((item, index) => (
						<div className="col-12" key={index}>
							<div className="form-check">
								<input
									className="form-check-input"
									{...register("timeCommitmentOfDeveloper", {
										required: true,
									})}
									type="radio"
									checked={
										watch("timeCommitmentOfDeveloper") === item.code
											? true
											: false
									}
									value={item.code}
									defaultValue={watch("timeCommitmentOfDeveloper")}
									onChange={(e) =>
										handleChange("timeCommitmentOfDeveloper", item.code)
									}
								/>
								<label className="form-check-label" htmlFor={item.code}>
									{item.name}
								</label>
							</div>
						</div>
					))}
					{errors?.timeCommitmentOfDeveloper && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.timeCommitmentOfDeveloper.type}
						</small>
					)}
				</div>

				<div className="form-actions">
					<button
						type="button"
						className="btn btn-sm btn-success mx-2"
						onClick={() => stepChange(3)}
					>
						Previous
					</button>
					<button type="submit" className="btn btn-sm btn-primary">
						Continue
					</button>
				</div>
			</form>
		</div>
	);
};

export default FormFour;
