import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
	updatedata,
	resetForm,
} from "./../../reduxStore/actions/productAction";
import { toast } from "react-toastify";
import { useHistory } from "react-router";

const heardFrom = [
	{ code: "news/press", name: "News/Press" },
	{ code: "colleague/friend", name: "Colleague/Friend" },
	{ code: "email/newsletter", name: "Email/newsletter" },
	{ code: "search-engines", name: "Search Engines" },
	{ code: "linkedIn", name: "LinkedIn" },
	{ code: "twitter/Facebook/Instagram", name: "Twitter/Facebook/Instagram" },
	{ code: "directory", name: "Directory" },
	{ code: "other", name: "Other" },
];

const engineerProcess = [
	{ code: "directly-for-me", name: "Directly for me" },
	{
		code: "there-is-another-final-client",
		name: "There is another final client",
	},
];

const FormNine = ({ formData, stepChange, handleChange }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const {
		register,
		reset,
		formState: { errors },
		handleSubmit: handleSubmit,
		watch,
	} = useForm({
		mode: "all",
	});

	const onSubmit = (data) => {
		dispatch(updatedata(data));
	};

	const { loading, updateResponse, validations } = useSelector(
		({ mulitStepFormReducer }) => ({
			updateResponse: mulitStepFormReducer.addUpdateResponse.data,
			loading: mulitStepFormReducer.addUpdateResponse.loading,
			error: mulitStepFormReducer.addUpdateResponse.error,
		}),
		shallowEqual
	);

	useEffect(() => {
		if (Object.keys(formData).length > 0) {
			reset({
				leadId: formData.leadId,
				step: 10,
				referenceSource: formData?.referenceSource,
				personalOrClient: formData?.personalOrClient,
			});
		}
	}, [formData]);

	useEffect(() => {
		if (Object.keys(updateResponse).length > 0) {
			toast.success("Form Completed");
			dispatch(resetForm("data"));
			history.push("/multi-step-success");
		}
	}, [updateResponse]);

	return (
		<div className="form">
			<form onSubmit={handleSubmit(onSubmit)}>
				<h5 className="text-muted mb-3">
					You are all set! A senior Engineering Specialist will contact you
					within the next couple of hours.
				</h5>
				<div className="row">
					<div className="col col-12">
						<h4 className="">1. How did you first learn about BairesDev?</h4>
					</div>
				</div>
				<div className="row mb-4">
					{heardFrom.map((item, index) => (
						<div className="col-12" key={index}>
							<div className="form-check">
								<input
									className="form-check-input"
									{...register("referenceSource", {
										required: true,
									})}
									type="radio"
									checked={
										watch("referenceSource") === item.code ? true : false
									}
									value={item.code}
									defaultValue={watch("referenceSource")}
								/>
								<label className="form-check-label" htmlFor={item.code}>
									{item.name}
								</label>
							</div>
						</div>
					))}
					{errors?.referenceSource && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.referenceSource.type}
						</small>
					)}
				</div>

				<div className="row">
					<div className="col col-12">
						<h4 className="">
							2. Will the engineers be working directly for you or one of your
							clients?
						</h4>
					</div>
				</div>
				<div className="row mb-4">
					{engineerProcess.map((item, index) => (
						<div className="col-12" key={index}>
							<div className="form-check">
								<input
									className="form-check-input"
									{...register("personalOrClient", {
										required: true,
									})}
									type="radio"
									checked={
										watch("personalOrClient") === item.code ? true : false
									}
									value={item.code}
									defaultValue={watch("personalOrClient")}
									onChange={(select) =>
										handleChange("personalOrClient", item.code)
									}
								/>
								<label className="form-check-label" htmlFor={item.code}>
									{item.name}
								</label>
							</div>
						</div>
					))}
					{errors?.personalOrClient && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.personalOrClient.type}
						</small>
					)}
				</div>

				<div className="form-actions">
					<button
						type="button"
						className="btn btn-sm btn-success mx-2"
						onClick={() => stepChange(8)}
					>
						Previous
					</button>
					<button type="submit" className="btn btn-sm btn-primary">
						Save
					</button>
				</div>
			</form>
		</div>
	);
};

export default FormNine;
