import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
	postdata,
	updatedata,
	resetForm,
} from "./../../reduxStore/actions/productAction";
import { useHistory } from "react-router-dom";
const jwt = require("jsonwebtoken");

const FormOne = ({ formData, stepChange, handleChange, formId }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const {
		register,
		formState: { errors },
		handleSubmit,
		reset,
		watch,
	} = useForm();

	const onSubmit = (data) => {
		if (formId) {
			dispatch(updatedata(data));
		} else {
			data.step = 2;
			dispatch(postdata(data));
		}
	};

	const { loading, addResponse, validations } = useSelector(
		({ mulitStepFormReducer }) => ({
			addResponse: mulitStepFormReducer.addUpdateResponse.data,
			loading: mulitStepFormReducer.addUpdateResponse.loading,
			error: mulitStepFormReducer.addUpdateResponse.error,
		}),
		shallowEqual
	);

	useEffect(() => {
		if (Object.keys(formData).length > 0) {
			reset({
				leadId: formData.leadId,
				step: formData.step > 1 ? formData.step : 2,
				fullName: formData.fullName,
				email: formData.email,
				note: formData.note,
			});
		}
	}, [formData]);

	useEffect(() => {
		if (addResponse && Object.keys(addResponse).length > 0) {
			stepChange(2);
			history.replace(
				`/multi-step-form/${jwt.sign(addResponse.data.formId, "form")}`
			);
			dispatch(resetForm("data"));
		}
	}, [addResponse]);

	return (
		<div className="form">
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="mb-3">
					<label htmlFor="inputName" className="form-label">
						Name
					</label>
					<input
						{...register("fullName", {
							required: true,
						})}
						type="text"
						className="form-control"
						value={formData?.fullName}
						onChange={(e) => handleChange("fullName", e.target.value)}
					/>
					{errors?.fullName && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.fullName.type}
						</small>
					)}
				</div>

				<div className="mb-3">
					<label htmlFor="inputEmail" className="form-label">
						Email
					</label>
					<input
						{...register("email", {
							required: true,
						})}
						type="email"
						className="form-control"
						defaultValue={formData?.email}
						onChange={(e) => handleChange("email", e.target.value)}
					/>
					{errors?.email && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.email.type}
						</small>
					)}
				</div>
				<div className="mb-3">
					<label htmlFor="inputNote" className="form-label">
						What can we do for you?
					</label>
					<input
						{...register("note", {
							required: true,
						})}
						type="text"
						className="form-control"
						defaultValue={formData?.note}
						onChange={(e) => handleChange("note", e.target.value)}
					/>
					{errors?.note && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.note.type}
						</small>
					)}
				</div>
				<div className="form-actions">
					{/* <button type="submit" className="btn btn-sm btn-primary" onClick={()=>stepChange(2)}> */}
					<button
						type="submit"
						className="btn btn-sm btn-primary"
						disabled={loading}
					>
						Continue
					</button>
				</div>
			</form>
		</div>
	);
};

export default FormOne;
