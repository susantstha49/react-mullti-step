import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
	updatedata,
	resetForm,
} from "./../../reduxStore/actions/productAction";

const startDate = [
	{ code: "i'm-already-late", name: "I'm already late" },
	{ code: "immediately", name: "Immediately" },
	{
		code: "when-i-get-internal-funding/approval",
		name: "When I get internal funding/approval",
	},
	{ code: "specify-a-date", name: "Specify a date" },
	{ code: "i-don't-know", name: "I don't know" },
];
const FormSeven = ({ formData, stepChange, handleChange }) => {
	const dispatch = useDispatch();
	const {
		register,
		reset,
		formState: { errors },
		handleSubmit: handleSubmit,
		watch,
	} = useForm({
		mode: "all",
	});

	const onSubmit = (data) => {
		dispatch(updatedata(data));
	};

	const { loading, updateResponse, validations } = useSelector(
		({ mulitStepFormReducer }) => ({
			updateResponse: mulitStepFormReducer.addUpdateResponse.data,
			loading: mulitStepFormReducer.addUpdateResponse.loading,
			error: mulitStepFormReducer.addUpdateResponse.error,
		}),
		shallowEqual
	);

	useEffect(() => {
		if (Object.keys(formData).length > 0) {
			reset({
				leadId: formData.leadId,
				step: +formData.step > 7 ? formData.step : 8,
				expectedStartDate: formData?.expectedStartDate,
			});
		}
	}, [formData]);

	useEffect(() => {
		if (Object.keys(updateResponse).length > 0) {
			stepChange(8);
			dispatch(resetForm("data"));
		}
	}, [updateResponse]);

	return (
		<div className="form">
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row">
					<div className="col col-12">
						<h4 className="">What’s the expected start date?</h4>
					</div>
				</div>
				<div className="row mb-4">
					{startDate.map((item, index) => (
						<div className="col-12" key={index}>
							<div className="form-check">
								<input
									className="form-check-input"
									{...register("expectedStartDate", {
										required: true,
									})}
									type="radio"
									checked={
										watch("expectedStartDate") === item.code ? true : false
									}
									value={item.code}
									defaultValue={watch("expectedStartDate")}
									onChange={(e) => handleChange("expectedStartDate", item.code)}
								/>
								<label className="form-check-label" htmlFor={item.code}>
									{item.name}
								</label>
							</div>
						</div>
					))}
					{errors?.expectedStartDate && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.expectedStartDate.type}
						</small>
					)}
				</div>

				<div className="form-actions">
					<button
						type="button"
						className="btn btn-sm btn-success mx-2"
						onClick={() => stepChange(6)}
					>
						Previous
					</button>
					<button type="submit" className="btn btn-sm btn-primary">
						Continue
					</button>
				</div>
			</form>
		</div>
	);
};

export default FormSeven;
