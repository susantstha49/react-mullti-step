import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
	updatedata,
	resetForm,
} from "./../../reduxStore/actions/productAction";

const monthlyBudget = [
	{ code: "under-$10k", name: "Under $10k" },
	{ code: "$10k-to-$50k", name: "$10k to $50k" },
	{ code: "$50k-to-$200k", name: "$50k to $200k" },
	{ code: "more-than-$200k", name: "More than $200k" },
];
const FormSix = ({ formData, stepChange, handleChange }) => {
	const dispatch = useDispatch();
	const {
		register,
		reset,
		formState: { errors },
		handleSubmit: handleSubmit,
		watch,
	} = useForm({
		mode: "all",
	});

	const onSubmit = (data) => {
		dispatch(updatedata(data));
	};

	const { loading, updateResponse, validations } = useSelector(
		({ mulitStepFormReducer }) => ({
			updateResponse: mulitStepFormReducer.addUpdateResponse.data,
			loading: mulitStepFormReducer.addUpdateResponse.loading,
			error: mulitStepFormReducer.addUpdateResponse.error,
		}),
		shallowEqual
	);

	useEffect(() => {
		if (Object.keys(formData).length > 0) {
			reset({
				leadId: formData.leadId,
				step: +formData.step > 6 ? formData.step : 7,
				setMonthlyBudget: formData?.setMonthlyBudget,
			});
		}
	}, [formData]);

	useEffect(() => {
		if (Object.keys(updateResponse).length > 0) {
			stepChange(7);
			dispatch(resetForm("data"));
		}
	}, [updateResponse]);

	return (
		<div className="form">
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row">
					<div className="col col-12">
						<h4 className="">Do you have a set monthly budget?</h4>
					</div>
				</div>
				<div className="row mb-4">
					{monthlyBudget.map((item, index) => (
						<div className="col-12" key={index}>
							<div className="form-check">
								<input
									className="form-check-input"
									{...register("setMonthlyBudget", {
										required: true,
									})}
									type="radio"
									checked={
										watch("setMonthlyBudget") === item.code ? true : false
									}
									value={item.code}
									defaultValue={watch("setMonthlyBudget")}
									onChange={(e) => handleChange("setMonthlyBudget", item.code)}
								/>
								<label className="form-check-label" htmlFor={item.code}>
									{item.name}
								</label>
							</div>
						</div>
					))}
					{errors?.setMonthlyBudget && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.setMonthlyBudget.type}
						</small>
					)}
				</div>

				<div className="form-actions">
					<button
						type="button"
						className="btn btn-sm btn-success mx-2"
						onClick={() => stepChange(5)}
					>
						Previous
					</button>
					<button type="submit" className="btn btn-sm btn-primary">
						Continue
					</button>
				</div>
			</form>
		</div>
	);
};

export default FormSix;
