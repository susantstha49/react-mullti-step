import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
	updatedata,
	resetForm,
} from "./../../reduxStore/actions/productAction";

const noOfITProfessionals = [
	{ code: "1", name: "1" },
	{ code: "2-5", name: "2-5" },
	{ code: "6-10", name: "6-10" },
	{ code: "11-20", name: "11-20" },
	{ code: "more-than-20", name: "More than 20" },
	{ code: "i-dont-know", name: "I dont know" },
];
const FormThree = ({ formData, stepChange, handleChange, formId }) => {
	const dispatch = useDispatch();
	const {
		register,
		reset,
		formState: { errors },
		handleSubmit: handleSubmit,
		watch,
	} = useForm({
		mode: "all",
	});

	const onSubmit = (data) => {
		dispatch(updatedata(data));
	};

	const { loading, updateResponse, validations } = useSelector(
		({ mulitStepFormReducer }) => ({
			updateResponse: mulitStepFormReducer.addUpdateResponse.data,
			loading: mulitStepFormReducer.addUpdateResponse.loading,
			error: mulitStepFormReducer.addUpdateResponse.error,
		}),
		shallowEqual
	);

	useEffect(() => {
		if (Object.keys(formData).length > 0) {
			reset({
				leadId: formData.leadId,
				step: +formData.step > 3 ? formData.step : 4,
				noOfITProfessionals: formData?.noOfITProfessionals,
			});
		}
	}, [formData]);

	useEffect(() => {
		if (Object.keys(updateResponse).length > 0) {
			stepChange(4);
			dispatch(resetForm("data"));
		}
	}, [updateResponse]);

	return (
		<div className="form">
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row">
					<div className="col col-12">
						<h4 className="">How many IT professionals do you need?</h4>
					</div>
				</div>
				<div className="row mb-4">
					{noOfITProfessionals.map((item, index) => (
						<div className="col-12" key={index}>
							<div className="form-check">
								<input
									className="form-check-input"
									{...register("noOfITProfessionals", {
										required: true,
									})}
									type="radio"
									checked={
										watch("noOfITProfessionals") === item.code ? true : false
									}
									value={item.code}
									defaultValue={watch("noOfITProfessionals")}
									onChange={(e) =>
										handleChange("noOfITProfessionals", e.target.value)
									}
								/>
								<label className="form-check-label" htmlFor={item.code}>
									{item.name}
								</label>
							</div>
						</div>
					))}
					{errors?.noOfITProfessionals && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.noOfITProfessionals.type}
						</small>
					)}
				</div>

				<div className="form-actions">
					<button
						type="button"
						className="btn btn-sm btn-success mx-2"
						onClick={() => stepChange(2)}
					>
						Previous
					</button>
					<button type="submit" className="btn btn-sm btn-primary">
						Continue
					</button>
				</div>
			</form>
		</div>
	);
};

export default FormThree;
