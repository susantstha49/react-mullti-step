import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
	updatedata,
	resetForm,
} from "./../../reduxStore/actions/productAction";

const serviceArr = [
	{
		code: "staff-augmentation",
		name: "Staff Augmentation",
	},
	{
		code: "autonomous-team",
		name: "Autonomous Team",
	},
	{
		code: "other",
		name: "Other",
	},
];

const FormTwo = ({ formData, stepChange, handleChange, formId }) => {
	const dispatch = useDispatch();
	const {
		register,
		reset,
		formState: { errors },
		handleSubmit,
		watch,
	} = useForm({
		mode: "all",
	});
	const onSubmit = (data) => {
		dispatch(updatedata(data));
	};

	const { loading, updateResponse, getResponse, validations } = useSelector(
		({ mulitStepFormReducer }) => ({
			updateResponse: mulitStepFormReducer.addUpdateResponse.data,
			loading: mulitStepFormReducer.addUpdateResponse.loading,
			error: mulitStepFormReducer.addUpdateResponse.error,
			getResponse: mulitStepFormReducer.getResponse.data,
		}),
		shallowEqual
	);

	useEffect(() => {
		if (Object.keys(formData).length > 0) {
			reset({
				leadId: formData.leadId,
				step: +formData.step > 2 ? formData.step : 3,
				serviceType: formData.serviceType,
				companyName: formData.companyName,
				phoneNo: formData.phoneNo,
			});
		}
	}, [formData]);

	useEffect(() => {
		if (Object.keys(updateResponse).length > 0) {
			stepChange(3);
			dispatch(resetForm("data"));
		}
	}, [updateResponse]);

	return (
		<div className="form">
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row">
					<div className="col col-12">
						<h4 className="">Select your service</h4>
					</div>
				</div>
				<div className="row mb-4">
					{serviceArr.map((service, index) => (
						<div className="col-12" key={index}>
							<div className="form-check">
								<input
									{...register("serviceType", {
										required: true,
									})}
									className="form-check-input"
									type="radio"
									checked={watch("serviceType") === service.code ? true : false}
									value={service.code}
									defaultValue={formData.serviceType}
									onChange={(e) => handleChange("serviceType", e.target.value)}
								/>
								<label className="form-check-label" htmlFor={service.code}>
									{service.name}
								</label>
							</div>
						</div>
					))}
					{errors?.serviceType && (
						<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
							{errors.serviceType.type}
						</small>
					)}
				</div>
				<div className="row">
					<div className="col col-12">
						<h4 className=" ">Contact Information</h4>
					</div>
				</div>
				<div className="row">
					<div className="col-12">
						<div className="mb-3">
							<label htmlFor="input-4" className="form-label">
								Company Name
							</label>
							<input
								{...register("companyName", {
									required: true,
								})}
								type="text"
								className="form-control"
								defaultValue={formData?.companyName}
							/>
							{errors?.companyName && (
								<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
									{errors.companyName.type}
								</small>
							)}
						</div>
					</div>
					<div className="col-12">
						<div className="mb-3">
							<label htmlFor="input-5" className="form-label">
								Phone Number
							</label>
							<input
								{...register("phoneNo", {
									required: true,
								})}
								type="text"
								className="form-control"
								defaultValue={formData?.phoneNo}
							/>
							{errors?.phoneNo && (
								<small className="text-danger mt-1 text-end d-inline-block mb-0 ">
									{errors.phoneNo.type}
								</small>
							)}
						</div>
					</div>
				</div>

				<div className="form-actions">
					<button
						type="button"
						className="btn btn-sm btn-success mx-2"
						onClick={() => stepChange(1)}
					>
						Previous
					</button>
					<button
						type="submit"
						className="btn btn-sm btn-primary "
						// onClick={() => stepChange(3)}
					>
						Continue
					</button>
				</div>
			</form>
		</div>
	);
};

export default FormTwo;
