import React, { useEffect, useState } from "react";
import FormOne from "./forms/FormOne";
import FormTwo from "./forms/FormTwo";
import FormThree from "./forms/FormThree";
import FormFour from "./forms/FormFour";
import FormFive from "./forms/FormFive";
import FormSix from "./forms/FormSix";
import FormSeven from "./forms/FormSeven";
import FormEight from "./forms/FormEight";
import FormNine from "./forms/FormNine";
import { useParams, useHistory } from "react-router-dom";
import { getData, reset } from "../reduxStore/actions/productAction";
import { fromStepValue } from "./constant";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { toast } from "react-toastify";
const jwt = require("jsonwebtoken");

const Index = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const params = useParams();
  const [activeState, setActiveState] = useState(0);
  const [formId, setFormId] = useState(null);

  const [formData, setFormData] = useState({});

  const { loading, getResponse, validations } = useSelector(
    ({ mulitStepFormReducer }) => ({
      getResponse: mulitStepFormReducer.getResponse.data,
      loading: mulitStepFormReducer.getResponse.loading,
      error: mulitStepFormReducer.getResponse.error,
    }),
    shallowEqual
  );

  const handleContinue = (name, value) => {
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  useEffect(() => {
    if (params?.id) {
      if (Object.keys(getResponse).length > 0) {
        if (+getResponse?.data?.step == 0 || getResponse?.data?.step > 9) {
          history.push(`/multi-step-form`);
          setFormData({});
          setActiveState(1);
        } else {
          setActiveState(+getResponse?.data?.step);
          setFormData(getResponse?.data);
        }
      }
    } else {
      setActiveState(1);
    }
  }, [getResponse]);

  useEffect(() => {
    if (params?.id) {
      try {
        var decoded = jwt.verify(params?.id, "form");
        dispatch(getData(decoded));
        setFormId(decoded);
      } catch (err) {
        toast.warn("Invalid token id");
        history.push(`/multi-step-form`);
        setActiveState(1);
      }
      // const base64Rejex =
      // 	/^(?:[A-Z0-9+\/]{4})*(?:[A-Z0-9+\/]{2}==|[A-Z0-9+\/]{3}=|[A-Z0-9+\/]{4})$/i;
      // const isBase64Valid = base64Rejex.test(params?.id); // base64Data is the base64 string
      // if (isBase64Valid) {

      // }
    }
  }, [params?.id]);

  const renderForm = (state) => {
    switch (state) {
      case 1:
        return (
          <FormOne
            formData={formData}
            stepChange={(param) => setActiveState(param)}
            handleChange={handleContinue}
            formId={formId}
          />
        );
      case 2:
        return (
          <FormTwo
            formData={formData}
            stepChange={(param) => setActiveState(param)}
            handleChange={handleContinue}
            formId={formId}
          />
        );
      case 3:
        return (
          <FormThree
            formData={formData}
            stepChange={(param) => setActiveState(param)}
            handleChange={handleContinue}
            formId={formId}
          />
        );
      case 4:
        return (
          <FormFour
            formData={formData}
            stepChange={(param) => setActiveState(param)}
            handleChange={handleContinue}
            formId={formId}
          />
        );
      case 5:
        return (
          <FormFive
            formData={formData}
            stepChange={(param) => setActiveState(param)}
            handleChange={handleContinue}
            formId={formId}
          />
        );
      case 6:
        return (
          <FormSix
            formData={formData}
            stepChange={(param) => setActiveState(param)}
            handleChange={handleContinue}
            formId={formId}
          />
        );
      case 7:
        return (
          <FormSeven
            formData={formData}
            stepChange={(param) => setActiveState(param)}
            handleChange={handleContinue}
            formId={formId}
          />
        );
      case 8:
        return (
          <FormEight
            formData={formData}
            stepChange={(param) => setActiveState(param)}
            handleChange={handleContinue}
            formId={formId}
          />
        );
      case 9:
        return (
          <FormNine
            formData={formData}
            stepChange={(param) => setActiveState(param)}
            handleChange={handleContinue}
            formId={formId}
          />
        );
      default:
        return "";
    }
  };
  return (
    <div>
      {/* form text description*/}
      {/* <div className="container">
        <div className="formWrapper-description text-center pt-5 pb-3">
          <h2 className="fw-bold">Free Savings Estimate</h2>
        </div>
      </div> */}

      <div className="container">
        <div className="formWrapper my-5">
          <div className="wizard">
            <div className="progress--bar">
              <div className="progress--bar-text">
                Step <span className="CurrentStep">{activeState}</span> of 9
              </div>
            </div>
            {renderForm(activeState)}
          </div>
        </div>
      </div>
      <div className="container " style={{ background: "#5D2B84" }}>
        <ul className="companies__wrapper text-center list-unstyled d-flex align-items-center justify-content-center">
          <li className="px-3 py-5">
            <img
              style={{ filter: "invert(1)" }}
              src="https://trobustech.com/wp-content/uploads/2021/06/lightweb-Group.png"
              alt="Johnson &amp; Johnson"
            />
          </li>
          <li className="p-3">
            <img
              style={{ filter: "invert(1)" }}
              src="https://trobustech.com/wp-content/uploads/2021/06/Clear1.png"
              alt="Urban Outfitters"
            />
          </li>
          <li className="p-3">
            <img
              style={{ filter: "invert(1)" }}
              src="https://trobustech.com/wp-content/uploads/2021/06/abis.png"
              alt="Autodesk"
            />
          </li>
          <li className="p-3">
            <img
              style={{ filter: "invert(1)" }}
              src="https://trobustech.com/wp-content/uploads/2021/06/awsfinal.png"
              alt="Univision"
            />
          </li>

          <li className="p-3">
            <img
              style={{ filter: "invert(1)" }}
              src="https://trobustech.com/wp-content/uploads/2021/06/microsoftpartner.png"
              alt="Nexroll"
            />
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Index;
