import React from "react";

function SuccessMessage() {
  return (
    <div className="container text-center py-5  mt-5">
      <div className="p-5 bg-white mt-5 d-inline-block ">
        <h4 className="mb-3">Thankyou for your information</h4>
        <a href="https://trobustech.com" className="btn btn-success">
          Visit our website
        </a>
      </div>
    </div>
  );
}

export default SuccessMessage;
