import Axios from "axios";
import {
	MULTISTEP_FORM_SUCCESS,
	MULTISTEP_FORM_START,
	MULTISTEP_FORM_FAIL,
	MULTISTEP_FORM_RESET,
	GET_MULTISTEP_FORM_SUCCESS,
	GET_MULTISTEP_FORM_START,
	GET_MULTISTEP_FORM_FAIL,
} from "../actionTypes";
import { API_ENDPOINTS } from "../apiEndPoints";
import { toast } from "react-toastify";

export const getData = (id) => {
	return (dispatch) => {
		dispatch({
			type: GET_MULTISTEP_FORM_START,
		});
		Axios(`${API_ENDPOINTS.FORM_POSTS_API}/${id}`, {
			method: "GET",
			data: id,
		})
			.then((res) => {
				const { data, status } = res;
				if (status === 200) {
					dispatch({
						type: GET_MULTISTEP_FORM_SUCCESS,
						payload: data,
					});
				} else {
					dispatch({
						type: GET_MULTISTEP_FORM_FAIL,
						payload: data,
					});
					toast.error("Error");
				}
			})
			.catch((e) => {
				dispatch({
					type: GET_MULTISTEP_FORM_FAIL,
					payload: e,
				});
				toast.error("Error");
			});
	};
};

export const postdata = (data) => {
	return (dispatch) => {
		dispatch({
			type: MULTISTEP_FORM_START,
		});
		Axios(`${API_ENDPOINTS.FORM_POSTS_API}`, {
			method: "POST",
			data: data,
		})
			.then((res) => {
				const { data, status } = res;
				if (status === 200) {
					dispatch({
						type: MULTISTEP_FORM_SUCCESS,
						payload: data,
					});
				} else {
					dispatch({
						type: MULTISTEP_FORM_FAIL,
						payload: data,
					});
					toast.error("Error");
				}
			})
			.catch((e) => {
				dispatch({
					type: MULTISTEP_FORM_FAIL,
					payload: e,
				});
				toast.error("Error");
			});
	};
};

export const updatedata = (data) => {
	return (dispatch) => {
		dispatch({
			type: MULTISTEP_FORM_START,
		});
		Axios(`${API_ENDPOINTS.FORM_POSTS_API}`, {
			method: "PUT",
			data: data,
		})
			.then((res) => {
				const { data, status } = res;
				if (status === 200) {
					dispatch({
						type: MULTISTEP_FORM_SUCCESS,
						payload: data,
					});
				} else {
					dispatch({
						type: MULTISTEP_FORM_FAIL,
						payload: data,
					});
					toast.error("Error");
				}
			})
			.catch((e) => {
				dispatch({
					type: MULTISTEP_FORM_FAIL,
					payload: e,
				});
				toast.error("Error");
			});
	};
};

export const resetForm = (data) => {
	return {
		type: MULTISTEP_FORM_RESET,
		payload: data,
	};
};
