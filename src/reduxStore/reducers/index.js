import { combineReducers } from "redux";
import { mulitStepFormReducer } from "./mulitStepFormReducer";

const reducers = combineReducers({
	mulitStepFormReducer: mulitStepFormReducer,
});

export default reducers;
