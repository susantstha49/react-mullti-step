import {
	MULTISTEP_FORM_SUCCESS,
	MULTISTEP_FORM_VALIDATION,
	MULTISTEP_FORM_START,
	MULTISTEP_FORM_FAIL,
	MULTISTEP_FORM_RESET,
	GET_MULTISTEP_FORM_SUCCESS,
	GET_MULTISTEP_FORM_START,
	GET_MULTISTEP_FORM_FAIL,
} from "../actionTypes";
const initialState = {
	stepId: 0,
	addUpdateResponse: {
		data: {},
		error: {},
		validation: {},
		loading: false,
	},
	getResponse: {
		data: {},
		error: {},
		loading: false,
	},
};

export const mulitStepFormReducer = (
	state = initialState,
	{ type, payload }
) => {
	switch (type) {
		case MULTISTEP_FORM_START:
			return {
				...state,
				addUpdateResponse: {
					...state.addUpdateResponse,
					loading: true,
					error: {},
				},
			};

		case MULTISTEP_FORM_SUCCESS:
			return {
				...state,
				addUpdateResponse: {
					...state.addUpdateResponse,
					data: payload,
					stepId: payload,
					validation: {},
					loading: false,
				},
			};

		case MULTISTEP_FORM_VALIDATION:
			return {
				...state,
				addUpdateResponse: {
					...state.addUpdateResponse,
					validation: payload,
					loading: false,
				},
			};

		case MULTISTEP_FORM_FAIL:
			return {
				...state,
				addUpdateResponse: {
					...state.addUpdateResponse,
					error: payload,
					loading: false,
				},
			};

		case GET_MULTISTEP_FORM_START:
			return {
				...state,
				getResponse: {
					...state.getResponse,
					loading: true,
					error: {},
				},
			};

		case GET_MULTISTEP_FORM_SUCCESS:
			return {
				...state,
				getResponse: {
					...state.getResponse,
					data: payload,
					stepId: payload,
					loading: false,
				},
			};

		case GET_MULTISTEP_FORM_FAIL:
			return {
				...state,
				getResponse: {
					...state.getResponse,
					error: payload,
					loading: false,
				},
			};

		case MULTISTEP_FORM_RESET:
			switch (payload) {
				case "data":
					return {
						...state,
						addUpdateResponse: initialState.addUpdateResponse,
					};
				default:
					return initialState;
			}
		default:
			return state;
	}
};
